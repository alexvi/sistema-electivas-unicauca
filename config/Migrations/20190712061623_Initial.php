<?php
use Migrations\AbstractMigration;

class Initial extends AbstractMigration
{
    public function up()
    {

        $this->table('classrooms', ['id' => false, 'primary_key' => ['cla_id']])
            ->addColumn('cla_id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('cla_code', 'string', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('numero_sal', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addIndex(
                [
                    'cla_code',
                ],
                ['unique'=> true]
            )
            ->create();

        $this->table('codes_students', ['id' => false, 'primary_key' => ['cod_est_id']])
            ->addColumn('cod_est_id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('est_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('cod_est_email', 'string', [
                'default' => null,
                'limit' => 30,
                'null' => false,
            ])
            ->addColumn('cod_est_token', 'string', [
                'default' => null,
                'limit' => 30,
                'null' => false,
            ])
            ->addColumn('active','boolean',[
                'default' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'est_id',
                ],
            )
            ->create();

        $this->table('codes_teachers', ['id' => false, 'primary_key' => ['cod_tea_id']])
            ->addColumn('cod_tea_id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('tea_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('cod_tea_email', 'string', [
                'default' => null,
                'limit' => 30,
                'null' => false,
            ])
            ->addColumn('cod_tea_token', 'string', [
                'default' => null,
                'limit' => 30,
                'null' => false,
            ])
            ->addColumn('active','boolean',[
                'default' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'tea_id',
                ]
            )
            ->create();

        $this->table('rel_est_mat', ['id' => false, 'primary_key' => ['est_id', 'sub_id']])
            ->addColumn('est_id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('sub_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addIndex(
                [
                    'est_id',
                ]
            )
            ->addIndex(
                [
                    'sub_id',
                ]
            )
            ->create();

        $this->table('rel_mat_sal', ['id' => false, 'primary_key' => ['sub_id', 'cla_id']])
            ->addColumn('sub_id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('cla_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addIndex(
                [
                    'cla_id',
                ]
            )
            ->addIndex(
                [
                    'sub_id',
                ]
            )
            ->create();

        $this->table('students', ['id' => false, 'primary_key' => ['est_id']])
            ->addColumn('est_id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('est_code', 'string', [
                'default' => null,
                'limit' => 12,
                'null' => false,
            ])
            ->addColumn('est_name', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('est_email', 'string', [
                'default' => null,
                'limit' => 30,
                'null' => false,
            ])
            ->addIndex(
                [
                    'est_code',
                ],
                ['unique'=> true]
            )
            ->create();

        $this->table('subjects', ['id' => false, 'primary_key' => ['sub_id']])
            ->addColumn('sub_id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('tea_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('sub_code', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('sub_name', 'string', [
                'default' => null,
                'limit' => 80,
                'null' => false,
            ])
            ->addColumn('sub_program', 'string', [
                'default' => null,
                'limit' => 30,
                'null' => false,
            ])
            ->addColumn('sub_estate', 'boolean', [
                'default' => true,
                'null' => false,
            ])
            ->addIndex(
                [
                    'sub_code',
                ],
                ['unique'=> true]
            )
            ->create();

        $this->table('teachers', ['id' => false, 'primary_key' => ['tea_id']])
            ->addColumn('tea_id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('tea_code', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => false,
            ])
            ->addColumn('tea_name', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('tea_email', 'string', [
                'default' => null,
                'limit' => 30,
                'null' => false,
            ])
            ->addIndex(
                [
                    'tea_code',
                ],
                ['unique'=> true]
            )
            ->create();

        $this->table('teacherschedules', ['id' => false, 'primary_key' => ['teasche_id']])
            ->addColumn('teasche_id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('tea_id', 'integer',[
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('sub_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('sche_sche', 'string', [
                'default' => null,
                'limit' => 30,
                'null' => false,
            ])
            ->create();

        $this->table('studentschedules', ['id' => false, 'primary_key' => ['stusche_id']])
            ->addColumn('stusche_id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('est_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('sub_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('sche_sche', 'string', [
                'default' => null,
                'limit' => 30,
                'null' => false,
            ])
            ->create();

        $this->table('users')
            ->addColumn('username', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('password', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addIndex(
                [
                    'username',
                ],
                ['unique'=> true]
            )
            ->create();

        $this->table('codes_students')
            ->addForeignKey(
                'est_id',
                'students',
                'est_id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('codes_teachers')
            ->addForeignKey(
                'tea_id',
                'teachers',
                'tea_id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('rel_est_mat')
            ->addForeignKey(
                'est_id',
                'students',
                'est_id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'sub_id',
                'subjects',
                'sub_id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('rel_mat_sal')
            ->addForeignKey(
                'cla_id',
                'classrooms',
                'cla_id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'sub_id',
                'subjects',
                'sub_id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('studentschedules')
            ->addForeignKey(
                'est_id',
                'students',
                'est_id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'sub_id',
                'subjects',
                'sub_id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('teacherschedules')
            ->addForeignKey(
                'tea_id',
                'teachers',
                'tea_id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'sub_id',
                'subjects',
                'sub_id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('subjects')
            ->addForeignKey(
                'tea_id',
                'teachers',
                'tea_id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();
    }

    public function down()
    {
        $this->table('codes_students')
            ->dropForeignKey(
                'est_id'
            )->save();

        $this->table('codes_teachers')
            ->dropForeignKey(
                'tea_id'
            )->save();

        $this->table('rel_est_mat')
            ->dropForeignKey(
                'est_id'
            )
            ->dropForeignKey(
                'sub_id'
            )->save();

        $this->table('rel_mat_sal')
            ->dropForeignKey(
                'cla_id'
            )
            ->dropForeignKey(
                'sub_id'
            )->save();

        $this->table('subjects')
            ->dropForeignKey(
                'tea_id'
            )->save();
        
        $this->table('studentschedules')
            ->dropForeignKey(
                'est_id'
            )
            ->dropForeignKey(
                'sub_id'
            )->save();

        $this->table('teacherschedules')
            ->dropForeignKey(
                'tea_id'
            )
            ->dropForeignKey(
                'sub_id'
            )->save();

        $this->table('classrooms')->drop()->save();
        $this->table('codes_students')->drop()->save();
        $this->table('codes_teachers')->drop()->save();
        $this->table('rel_est_mat')->drop()->save();
        $this->table('rel_mat_sal')->drop()->save();
        $this->table('students')->drop()->save();
        $this->table('subjects')->drop()->save();
        $this->table('teachers')->drop()->save();
        $this->table('users')->drop()->save();
        $this->table('studentschedules')->drop()->save();
        $this->table('teacherschedules')->drop()->save();
    }
}
