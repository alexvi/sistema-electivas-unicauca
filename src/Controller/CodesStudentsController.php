<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CodesStudents Controller
 *
 * @property \App\Model\Table\CodesStudentsTable $CodesStudents
 *
 * @method \App\Model\Entity\CodesStudent[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CodesStudentsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Students']
        ];
        $codesStudents = $this->paginate($this->CodesStudents);

        $this->set(compact('codesStudents'));
    }

    /**
     * View method
     *
     * @param string|null $id Codes Student id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $codesStudent = $this->CodesStudents->get($id, [
            'contain' => ['Students']
        ]);

        $this->set('codesStudent', $codesStudent);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $codesStudent = $this->CodesStudents->newEntity();
        if ($this->request->is('post')) {
            $codesStudent = $this->CodesStudents->patchEntity($codesStudent, $this->request->getData());
            if ($this->CodesStudents->save($codesStudent)) {
                $this->Flash->success(__('The codes student has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The codes student could not be saved. Please, try again.'));
        }
        $students = $this->CodesStudents->Students->find('list', ['limit' => 200]);
        $this->set(compact('codesStudent', 'students'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Codes Student id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $codesStudent = $this->CodesStudents->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $codesStudent = $this->CodesStudents->patchEntity($codesStudent, $this->request->getData());
            if ($this->CodesStudents->save($codesStudent)) {
                $this->Flash->success(__('The codes student has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The codes student could not be saved. Please, try again.'));
        }
        $students = $this->CodesStudents->Students->find('list', ['limit' => 200]);
        $this->set(compact('codesStudent', 'students'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Codes Student id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $codesStudent = $this->CodesStudents->get($id);
        if ($this->CodesStudents->delete($codesStudent)) {
            $this->Flash->success(__('The codes student has been deleted.'));
        } else {
            $this->Flash->error(__('The codes student could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
