<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CodesStudent Entity
 *
 * @property int $cod_est_id
 * @property int|null $est_id
 * @property string $cod_est_email
 * @property string $cod_est_token
 * @property bool $active
 *
 * @property \App\Model\Entity\Student $student
 */
class CodesStudent extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'est_id' => true,
        'cod_est_email' => true,
        'cod_est_token' => true,
        'active' => true,
        'student' => true
    ];
}
