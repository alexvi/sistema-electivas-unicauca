<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Subject Entity
 *
 * @property int $sub_id
 * @property int|null $tea_id
 * @property string $sub_code
 * @property string $sub_name
 * @property string $sub_program
 * @property bool $sub_estate
 *
 * @property \App\Model\Entity\Teacher $teacher
 */
class Subject extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tea_id' => true,
        'sub_code' => true,
        'sub_name' => true,
        'sub_program' => true,
        'sub_estate' => true,
        'teacher' => true
    ];
}
