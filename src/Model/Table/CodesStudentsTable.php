<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CodesStudents Model
 *
 * @property \App\Model\Table\StudentsTable&\Cake\ORM\Association\BelongsTo $Students
 *
 * @method \App\Model\Entity\CodesStudent get($primaryKey, $options = [])
 * @method \App\Model\Entity\CodesStudent newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CodesStudent[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CodesStudent|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CodesStudent saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CodesStudent patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CodesStudent[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CodesStudent findOrCreate($search, callable $callback = null, $options = [])
 */
class CodesStudentsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('codes_students');
        $this->setDisplayField('cod_est_id');
        $this->setPrimaryKey('cod_est_id');

        $this->belongsTo('Students', [
            'foreignKey' => 'est_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('cod_est_id')
            ->allowEmptyString('cod_est_id', null, 'create');

        $validator
            ->scalar('cod_est_email')
            ->maxLength('cod_est_email', 30)
            ->requirePresence('cod_est_email', 'create')
            ->notEmptyString('cod_est_email');

        $validator
            ->scalar('cod_est_token')
            ->maxLength('cod_est_token', 30)
            ->requirePresence('cod_est_token', 'create')
            ->notEmptyString('cod_est_token');

        $validator
            ->boolean('active')
            ->requirePresence('active', 'create')
            ->notEmptyString('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['est_id'], 'Students'));

        return $rules;
    }
}
