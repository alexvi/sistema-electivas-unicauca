<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Classroom $classroom
 */
?>
<div class="classrooms view large-9 medium-8 columns content">
    <h3>Salon <?= h($classroom->numero_sal) ?></h3>
    <table class="table">
        <tr>
            <th scope="row"><?= __('Identificador') ?></th>
            <td><?= $this->Number->format($classroom->cla_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Código de salón') ?></th>
            <td><?= h($classroom->cla_code) ?></td>
        </tr>
    </table>
</div>
