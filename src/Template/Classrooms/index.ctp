<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Classroom[]|\Cake\Collection\CollectionInterface $classrooms
 */
?>
<div class="classrooms index large-9 medium-8 columns content">
    <h3><?= __('Salones') ?></h3>
    <table class="table" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('cla_id',['label'=>'id']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('cla_code',['label'=>'Código']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('numero_sal',['label'=>'Número de salón']) ?></th>
                <th scope="col" class="actions"><?= __('Opciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($classrooms as $classroom): ?>
            <tr>
                <td><?= $this->Number->format($classroom->cla_id) ?></td>
                <td><?= h($classroom->cla_code) ?></td>
                <td><?= h($classroom->numero_sal) ?></td>
                <td class="actions">
                <?= $this->Html->link('Ver', ['action' => 'view', $classroom->cla_id],['class'=>'btn btn-success']) ?>
                    <?= $this->Html->link('Editar', ['action' => 'edit', $classroom->cla_id],['class'=>'btn btn-info']) ?>
                    <?= $this->Form->postLink('Eliminar', ['action' => 'delete', $classroom->cla_id],['confirm' => __('Estas seguro de que quieres eliminar el salón {0}?', $classroom->numero_sal),'class'=> 'btn btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?= $this->Html->link('Registrar salón', ['action' => 'add'],['class'=>'btn btn-primary']) ?>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< Anterior') ?>
            <?= $this->Paginator->numbers(['before'=>'','after'=>'']) ?>
            <?= $this->Paginator->next('Siguiente >') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} registros de {{count}}')]) ?></p>
    </div>
</div>
