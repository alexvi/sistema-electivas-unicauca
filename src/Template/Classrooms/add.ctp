<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Classroom $classroom
 */
?>
<div class="classrooms form large-9 medium-8 columns content">
    <?= $this->Form->create($classroom) ?>
    <fieldset>
        <legend><?= __('Agregar Salón') ?></legend>
        <?php
            echo $this->Form->control('cla_code',['label'=>'Código']);
            echo $this->Form->control('numero_sal',['label'=>'Número de salón']);
        ?>
    </fieldset>
    <?= $this->Form->button('Agregar',['class'=>'btn btn-primary']) ?>
    <?= $this->Html->link('Volver', ['action' => 'index'],['class'=>'btn btn-info']) ?>
    <?= $this->Form->end() ?>
</div>
