<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Classroom $classroom
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $classroom->cla_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $classroom->cla_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Classrooms'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="classrooms form large-9 medium-8 columns content">
    <?= $this->Form->create($classroom) ?>
    <fieldset>
        <legend><?= __('Edit Classroom') ?></legend>
        <?php
            echo $this->Form->control('cla_code');
            echo $this->Form->control('numero_sal');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
