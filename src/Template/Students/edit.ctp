<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Student $student
 */
?>
<div class="students form large-9 medium-8 columns content">
    <?= $this->Form->create($student) ?>
    <fieldset>
        <legend><?= ('Editar estudiante') ?></legend>
        <?php
            echo $this->Form->control('est_code',['label'=>'Código']);
            echo $this->Form->control('est_name',['label'=>'Nombre']);
            echo $this->Form->control('est_email',['label'=>'Correo Institucional']);
        ?>
    </fieldset>
    <?= $this->Form->button('Guardar',['class'=>'btn btn-primary']) ?>
    <?= $this->Html->link('Volver', ['action' => 'index'],['class'=>'btn btn-info']) ?>
    <?= $this->Form->postLink('Eliminar', ['action' => 'delete', $student->est_id],['confirm' => __('Estas seguro de que quieres eliminar a {0}?', $student->est_name),'class'=> 'btn btn-danger']) ?>
    <?= $this->Form->end() ?>
</div>
