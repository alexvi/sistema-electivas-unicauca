<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Student[]|\Cake\Collection\CollectionInterface $students
 */
?>
<div class="students index large-9 medium-8 columns content">
    <h3><?= __('Estudiantes') ?></h3>
    <table class="table" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('est_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('est_code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('est_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('est_email') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($students as $student): ?>
            <tr>
                <td><?= $this->Number->format($student->est_id,['label'=>'Código']) ?></td>
                <td><?= h($student->est_code) ?></td>
                <td><?= h($student->est_name) ?></td>
                <td><?= h($student->est_email) ?></td>
                <td class="actions">
                <?= $this->Html->link('Ver', ['action' => 'view', $student->est_id],['class'=>'btn btn-success']) ?>
                    <?= $this->Html->link('Editar', ['action' => 'edit', $student->est_id],['class'=>'btn btn-info']) ?>
                    <?= $this->Form->postLink('Eliminar', ['action' => 'delete', $student->est_id],['confirm' => __('Estas seguro de que quieres eliminar a {0}?', $student->est_name),'class'=> 'btn btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?= $this->Html->link('Registrar Estudiante', ['action' => 'add'],['class'=>'btn btn-primary']) ?>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< Anterior') ?>
            <?= $this->Paginator->numbers(['before'=>'','after'=>'']) ?>
            <?= $this->Paginator->next('Siguiente >') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} registros de {{count}}')]) ?></p>
    </div>
</div>
