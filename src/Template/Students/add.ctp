<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Student $student
 */
?>
<div class="students form large-9 medium-8 columns content">
    <?= $this->Form->create($student) ?>
    <fieldset>
        <legend><?= __('Agregar Estudiante') ?></legend>
        <?php
            echo $this->Form->control('est_code',['label'=>'Código']);
            echo $this->Form->control('est_name',['label'=>'Nombre']);
            echo $this->Form->control('est_email',['label'=>'Correo Intitucional']);
        ?>
    </fieldset>
    <?= $this->Form->button('Agregar',['class'=>'btn btn-primary']) ?>
    <?= $this->Html->link('Volver', ['action' => 'index'],['class'=>'btn btn-info']) ?>
    <?= $this->Form->end() ?>
</div>
