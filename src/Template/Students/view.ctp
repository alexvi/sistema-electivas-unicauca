<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Student $student
 */
?>

<div class="students view large-9 medium-8 columns content">
    <h3><?= h($student->est_name) ?></h3>
    <table class="table">
        <tr>
            <th scope="row"><?=('Correo Institucional: ') ?></th>
            <td><?= h($student->est_email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?=('Consecutivo almacenado: ') ?></th>
            <td><?= $this->Number->format($student->est_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Código: ') ?></th>
            <td><?= h($student->est_code) ?></td>
        </tr>
    </table>
</div>
<?= $this->Html->link('Volver', ['action' => 'index'],['class'=>'btn btn-info']) ?>