<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Teacher $teacher
 */
?>

<div class="teachers view large-9 medium-8 columns content">
    <h3><?= h($teacher->tea_name) ?></h3>
    <table class="table">
        <tr>
            <th scope="row"><?= __('Correo Institucional') ?></th>
            <td><?= h($teacher->tea_email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('id') ?></th>
            <td><?= $this->Number->format($teacher->tea_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Código') ?></th>
            <td><?= h($teacher->tea_code) ?></td>
        </tr>
    </table>
</div>
<?= $this->Html->link('Volver', ['action' => 'index'],['class'=>'btn btn-info']) ?>
