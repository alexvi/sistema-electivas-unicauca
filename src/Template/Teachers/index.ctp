<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Teacher[]|\Cake\Collection\CollectionInterface $teachers
 */
?>

<div class="teachers index large-9 medium-8 columns content">
    <h3><?= ('Profesores') ?></h3>
    <table class="table" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('tea_id',['label'=>'id']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('tea_code',['label'=>'Código']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('tea_name',['label'=>'Nombre']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('tea_email',['label'=>'Correo Institucional']) ?></th>
                <th scope="col" class="actions"><?= __('opciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($teachers as $teacher): ?>
            <tr>
                <td><?= $this->Number->format($teacher->tea_id) ?></td>
                <td><?= h($teacher->tea_code) ?></td>
                <td><?= h($teacher->tea_name) ?></td>
                <td><?= h($teacher->tea_email) ?></td>
                <td class="actions">
                <?= $this->Html->link('Ver', ['action' => 'view', $teacher->tea_id],['class'=>'btn btn-success']) ?>
                    <?= $this->Html->link('Editar', ['action' => 'edit', $teacher->tea_id],['class'=>'btn btn-info']) ?>
                    <?= $this->Form->postLink('Eliminar', ['action' => 'delete', $teacher->tea_id],['confirm' => __('Estas seguro de que quieres eliminar a {0}?', $teacher->tea_name),'class'=> 'btn btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?= $this->Html->link('Registrar profesor', ['action' => 'add'],['class'=>'btn btn-primary']) ?>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< Anterior') ?>
            <?= $this->Paginator->numbers(['before'=>'','after'=>'']) ?>
            <?= $this->Paginator->next('Siguiente >') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} registros de {{count}}')]) ?></p>
    </div>
</div>
