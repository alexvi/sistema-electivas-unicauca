<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Teacher $teacher
 */
?>
<div class="teachers form large-9 medium-8 columns content">
    <?= $this->Form->create($teacher) ?>
    <fieldset>
        <legend><?= __('Editar profesor') ?></legend>
        <?php
            echo $this->Form->control('tea_code',['label'=>'Código']);
            echo $this->Form->control('tea_name',['label'=>'Nombre']);
            echo $this->Form->control('tea_email',['label'=>'Correo Institucional']);
        ?>
    </fieldset>
    <?= $this->Form->button('Guardar',['class'=>'btn btn-primary']) ?>
    <?= $this->Html->link('Volver', ['action' => 'index'],['class'=>'btn btn-info']) ?>
    <?= $this->Form->postLink('Eliminar', ['action' => 'delete', $teacher->tea_id],['confirm' => __('Estas seguro de que quieres eliminar a {0}?', $teacher->tea_name),'class'=> 'btn btn-danger']) ?>
    <?= $this->Form->end() ?>
</div>
