<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Teacher $teacher
 */
?>

<div class="teachers form large-9 medium-8 columns content">
    <?= $this->Form->create($teacher) ?>
    <fieldset>
        <legend><?= __('Agregar Profesor') ?></legend>
        <?php
            echo $this->Form->control('tea_code',['label'=>'Código']);
            echo $this->Form->control('tea_name',['label'=>'Nombre']);
            echo $this->Form->control('tea_email',['label'=>'Correo Institucional']);
        ?>
    </fieldset>
    <?= $this->Form->button('Agregar',['class'=>'btn btn-primary']) ?>
    <?= $this->Html->link('Volver', ['action' => 'index'],['class'=>'btn btn-info']) ?>
    <?= $this->Form->end() ?>
</div>
