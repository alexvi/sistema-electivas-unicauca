<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CodesStudent[]|\Cake\Collection\CollectionInterface $codesStudents
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Codes Student'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Students'), ['controller' => 'Students', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Student'), ['controller' => 'Students', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="codesStudents index large-9 medium-8 columns content">
    <h3><?= __('Codes Students') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('cod_est_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('est_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cod_est_email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cod_est_token') ?></th>
                <th scope="col"><?= $this->Paginator->sort('active') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($codesStudents as $codesStudent): ?>
            <tr>
                <td><?= $this->Number->format($codesStudent->cod_est_id) ?></td>
                <td><?= $codesStudent->has('student') ? $this->Html->link($codesStudent->student->est_id, ['controller' => 'Students', 'action' => 'view', $codesStudent->student->est_id]) : '' ?></td>
                <td><?= h($codesStudent->cod_est_email) ?></td>
                <td><?= h($codesStudent->cod_est_token) ?></td>
                <td><?= h($codesStudent->active) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $codesStudent->cod_est_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $codesStudent->cod_est_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $codesStudent->cod_est_id], ['confirm' => __('Are you sure you want to delete # {0}?', $codesStudent->cod_est_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
