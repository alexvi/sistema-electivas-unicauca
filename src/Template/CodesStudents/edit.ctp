<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CodesStudent $codesStudent
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $codesStudent->cod_est_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $codesStudent->cod_est_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Codes Students'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Students'), ['controller' => 'Students', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Student'), ['controller' => 'Students', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="codesStudents form large-9 medium-8 columns content">
    <?= $this->Form->create($codesStudent) ?>
    <fieldset>
        <legend><?= __('Edit Codes Student') ?></legend>
        <?php
            echo $this->Form->control('est_id', ['options' => $students, 'empty' => true]);
            echo $this->Form->control('cod_est_email');
            echo $this->Form->control('cod_est_token');
            echo $this->Form->control('active');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
