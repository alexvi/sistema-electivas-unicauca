<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CodesStudent $codesStudent
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Codes Student'), ['action' => 'edit', $codesStudent->cod_est_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Codes Student'), ['action' => 'delete', $codesStudent->cod_est_id], ['confirm' => __('Are you sure you want to delete # {0}?', $codesStudent->cod_est_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Codes Students'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Codes Student'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Students'), ['controller' => 'Students', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Student'), ['controller' => 'Students', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="codesStudents view large-9 medium-8 columns content">
    <h3><?= h($codesStudent->cod_est_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Student') ?></th>
            <td><?= $codesStudent->has('student') ? $this->Html->link($codesStudent->student->est_id, ['controller' => 'Students', 'action' => 'view', $codesStudent->student->est_id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cod Est Email') ?></th>
            <td><?= h($codesStudent->cod_est_email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cod Est Token') ?></th>
            <td><?= h($codesStudent->cod_est_token) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cod Est Id') ?></th>
            <td><?= $this->Number->format($codesStudent->cod_est_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Active') ?></th>
            <td><?= $codesStudent->active ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
