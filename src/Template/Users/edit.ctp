<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= ('Editar Usuario') ?></legend>
        <?php
            echo $this->Form->control('username');
            echo $this->Form->control('password');
            echo $this->Form->control('name');
        ?>
    </fieldset>
    <?= $this->Form->button('Guardar',['class'=>'btn btn-primary']) ?>
    <?= $this->Html->link('Volver', ['action' => 'index'],['class'=>'btn btn-info']) ?>
    <?= $this->Form->postLink('Eliminar', ['action' => 'delete', $user->id],['confirm' => __('Estas seguro de que quieres eliminar a {0}?', $user->name),'class'=> 'btn btn-danger']) ?>
    <?= $this->Form->end() ?>
</div>
