
<?= $this->Html->css('logincss.css') ?>
<?= $this->fetch('css') ?>
<?= $this->layout="loginlay" ?>

<h1 class="title-agile text-center">Universidad del Cauca</h1>
<h1 class="title-agile text-center">Control de electivas</h1>
    <div class="content-w3ls">
        <div class="content-bottom">
			<?= $this->Form->create() ?>
            <img src="../img/logo.png" class="img" alt="Responsive image" style="display: block; margin-left: auto;
  margin-right: auto;" width ="40%">
				<div id= "title" style="text-align: center">
					<h2>Inicio de sesión</h2>
				</div>
                <div class="field-group">
                    <div class="wthree-field">
					<div class="form-group">
                        <?= $this->Form->input('username',['class'=>'form-control input-lg', 'placeholder'=>
                        'Correo Institucional', 'label'=>'','required'])?>
                    </div>
                    </div>
                </div>
                <div class="field-group">
                    <div class="wthree-field">
					<div class="form-group">
                        <?= $this->Form->input('password',['class'=>'form-control input-lg', 'placeholder'=>
                        'Contraseña', 'label'=>'','required'])?>
                    </div>
                    </div>
                </div>
                <div class="field-group">
                    <div class="check">
                        <label >
                            <input type="checkbox" onclick="myFunction()">
                            <i> </i>Mostrar Contraseña</label>
                    </div>
                    <!-- script for show password -->
                    <script>
                        function myFunction() {
                            var x = document.getElementById("password");
                            if (x.type === "password") {
                                x.type = "text";
                            } else {
                                x.type = "password";
                            }
                        }
                    </script>
                    <!-- //script for show password -->
                </div>
                <!---728x90--->
				<div style="text-align: center">
					<?= $this->Form->button('Iniciar sesion',['class'=>'btn btn-primary']) ?> 
				</div>
            <?= $this->Form->end() ?>
        </div>
    </div>
    <!---728x90--->
	<!--
    <div class="copyright text-center">
        <p>© 2018 Prime login Form. All rights reserved | Design by
            <a href="http://w3layouts.com">W3layouts</a>
        </p>
    </div>
	-->

