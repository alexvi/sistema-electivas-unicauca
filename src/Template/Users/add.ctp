<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Agregar usuario') ?></legend>
        <?php
            echo $this->Form->control('username', ['label'=>  'Correo']);
            echo $this->Form->control('password', ['label'=>  'Contraseña']);
            echo $this->Form->control('name', ['label'=>  'Nombre']);
        ?>
    </fieldset>
    <?= $this->Form->button('Agregar',['class'=>'btn btn-primary']) ?>
    <?= $this->Html->link('Volver', ['action' => 'index'],['class'=>'btn btn-info']) ?>
    <?= $this->Form->end() ?>
</div>
