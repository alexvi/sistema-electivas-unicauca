<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Subject $subject
 */
?>


<div class="subjects form large-9 medium-8 columns content">
    <?= $this->Form->create($subject) ?>
    <fieldset>
        <legend><?= __('Agregar Electiva') ?></legend>
        <span>Profesor</span>
        <select class="form-control" name="tea_id" label="id Profesor">
            <?php 
                foreach ($teachers as $teacher): ?>
                    <option value="<?= $teacher->tea_id?>"><?= $teacher->tea_name?></option>
            <?php endforeach; ?>
        </select>
        <?php
            echo $this->Form->control('sub_code',['label'=>'Código']);
            echo $this->Form->control('sub_name',['label'=>'Nombre']);
            echo $this->Form->control('sub_program',['label'=>'Programa']);
        ?>
    </fieldset>
    <?= $this->Form->button('Agregar',['class'=>'btn btn-primary']) ?>
    <?= $this->Html->link('Volver', ['action' => 'index'],['class'=>'btn btn-info']) ?>
    <?= $this->Form->end() ?>
</div>
