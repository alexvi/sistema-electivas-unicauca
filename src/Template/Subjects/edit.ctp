<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Subject $subject
 */
?>
<div class="subjects form large-9 medium-8 columns content">
    <?= $this->Form->create($subject) ?>
    <fieldset>
        <legend><?= __('Editar Electiva') ?></legend>
        <?php
            echo $this->Form->control('tea_id', ['options' => $teachers, 'empty' => true,'label'=>'id profesor']);
            echo $this->Form->control('sub_code',['label'=>'Código']);
            echo $this->Form->control('sub_name',['label'=>'Nombre']);
            echo $this->Form->control('sub_program',['label'=>'Programa']);
        ?>
    </fieldset>
    <?= $this->Form->button('Guardar',['class'=>'btn btn-primary']) ?>
    <?= $this->Html->link('Volver', ['action' => 'index'],['class'=>'btn btn-info']) ?>
    <?= $this->Form->postLink('Eliminar', ['action' => 'delete', $subject->sub_id],['confirm' => __('Estas seguro de que quieres eliminar a {0}?', $subject->sub_name),'class'=> 'btn btn-danger']) ?>
    <?= $this->Form->end() ?>
</div>
