<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Subject[]|\Cake\Collection\CollectionInterface $subjects
 */
?>

<div class="subjects index large-9 medium-8 columns content">
    <h3><?= __('Electivas') ?></h3>
    <table class="table" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('sub_id',['label'=>'id']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('tea_id',['label'=>'Profesor']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('sub_code',['label'=>'Código']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('sub_name',['label'=>'Nombre']) ?></th>
                <th scope="col"><?= $this->Paginator->sort('sub_program',['label'=>'Programa']) ?></th>
                <th scope="col" class="actions"><?= __('Opciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($subjects as $subject): ?>
            <tr>
                <td><?= $this->Number->format($subject->sub_id) ?></td>
                <td><?= $subject->has('teacher') ? $this->Html->link($subject->teacher->tea_name, ['controller' => 'Teachers', 'action' => 'view', $subject->teacher->tea_id]) : '' ?></td>
                <td><?= h($subject->sub_code) ?></td>
                <td><?= h($subject->sub_name) ?></td>
                <td><?= h($subject->sub_program) ?></td>
                <td class="actions">
                <?= $this->Html->link('Ver', ['action' => 'view', $subject->sub_id],['class'=>'btn btn-success']) ?>
                    <?= $this->Html->link('Editar', ['action' => 'edit', $subject->sub_id],['class'=>'btn btn-info']) ?>
                    <?= $this->Form->postLink('Eliminar', ['action' => 'delete',  $subject->sub_id],['confirm' => __('Estas seguro de que quieres eliminar a {0}?', $subject->sub_name),'class'=> 'btn btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?= $this->Html->link('Registrar electiva', ['action' => 'add'],['class'=>'btn btn-primary']) ?>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< Anterior') ?>
            <?= $this->Paginator->numbers(['before'=>'','after'=>'']) ?>
            <?= $this->Paginator->next('Siguiente >') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} registros de {{count}}')]) ?></p>
    </div>
</div>
