<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Subject $subject
 */
?>
<div class="subjects view large-9 medium-8 columns content">
    <h3><?= h($subject->sub_name) ?></h3>
    <table class="table">
        <tr>    
            <th scope="row"><?= __('Sub Id') ?></th>
            <td><?= $this->Number->format($subject->sub_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sub Code') ?></th>
            <td><?= h($subject->sub_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Profesor') ?></th>
            <td><?= $subject->has('teacher') ? $this->Html->link($subject->teacher->tea_name, ['controller' => 'Teachers', 'action' => 'view', $subject->teacher->tea_id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Programa') ?></th>
            <td><?= h($subject->sub_program) ?></td>
        </tr>
    </table>
</div>
<?= $this->Html->link('Volver', ['action' => 'index'],['class'=>'btn btn-info']) ?>
