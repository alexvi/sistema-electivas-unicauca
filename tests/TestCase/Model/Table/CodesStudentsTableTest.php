<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CodesStudentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CodesStudentsTable Test Case
 */
class CodesStudentsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CodesStudentsTable
     */
    public $CodesStudents;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CodesStudents',
        'app.Students'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CodesStudents') ? [] : ['className' => CodesStudentsTable::class];
        $this->CodesStudents = TableRegistry::getTableLocator()->get('CodesStudents', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CodesStudents);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
